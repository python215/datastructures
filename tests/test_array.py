import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from myarray import MyArray

def test_array():
    arr = MyArray('i', [1,2,3,4,5])
    l = arr.getArrayList()
    print(l)
    assert arr.getArrayList() == [1,2,3,4,5]
    assert arr.getValueAt(0) == 1
    assert arr.getValueAt(4) == 5
    arr.Append(6)
    arr.Append(7)
    l = arr.getArrayList()
    assert l == [1,2,3,4,5,6,7]

    arr.Insert(0,0)
    arr.Insert(7,8)
    arr.Insert(2,2)
    l = arr.getArrayList()
    assert l == [0,1,2,2,3,4,5,6,8,7]

    arr.Pop()
    assert arr.getArrayList() == [0,1,2,2,3,4,5,6,8]
    arr.Pop(0)
    assert arr.getArrayList() == [0,1,2,2,3,4,5,6]

    arr.Remove(2)
    assert arr.getArrayList() == [0,1,2,3,4,5,6]
    arr.Remove(0)
    assert arr.getArrayList() == [1,2,3,4,5,6]
    
    a = arr.Slice(0,5)
    l = []
    for i in range(0, len(a)):
        l.append(a[i])

    assert l == [1,2,3,4,5]

    assert arr.Find(5) == 4
    arr.Update(0,0)
    assert arr.getArrayList() == [0,2,3,4,5,6]

