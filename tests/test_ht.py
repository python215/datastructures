import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from myhashtable import HashTable

def test_ht():
    ht = HashTable(100)

    ht['Mar 31'] = 31
    ht['Feb 29'] = 29
    ht['Dec 31'] = 365
    ht['Jan 1'] = 1

    assert ht['Mar 31'] == 31
    assert ht['Feb 29'] == 29
    assert ht['Dec 31'] == 365
    assert ht['Jan 1'] == 1

    del ht['Jan 1']
    assert ht['Jan 1'] == None
