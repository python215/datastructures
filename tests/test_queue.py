import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from myqueue import MyQueue

def test_queue():
    myq = MyQueue()
    assert myq.Length() == 0

    myq.Push(1)
    myq.Push(2)
    assert myq.GetQ() == [2,1]

    assert myq.Peek() == 1

    assert myq.Pop() == 1
    assert myq.GetQ() == [2]

    myq.Push(1)
    assert myq.GetQ() == [1,2]

