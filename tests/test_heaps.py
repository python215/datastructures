import heapq
"""
A heap is created by using python’s inbuilt library named heapq. This library has the relevant functions to carry out various operations on heap data structure. Below is a list of these functions.

heapify − This function converts a regular list to a heap. In the resulting heap the smallest element gets pushed to the index position 0. But rest of the data elements are not necessarily sorted.

heappush − This function adds an element to the heap without altering the current heap.

heappop − This function returns the smallest data element from the heap.

heapreplace − This function replaces the smallest data element with a new value supplied in the function.
"""
def test_heapq():
    # Creating Heap
    H = [21, 1, 45, 78, 3, 5]
    # use heapify ro rearrange the list
    heapq.heapify(H)
    print(H)
    assert len(H) == 6
    assert H[0] == 1

    # Add element
    heapq.heappush(H,8)
    print(H)
    assert len(H) == 7

    # Remove element from the heap
    heapq.heappop(H)
    print(H)
    assert len(H) == 6
    assert H[0] == 3

    # Replace an element
    heapq.heapreplace(H,6)
    print(H)
    assert len(H) == 6
    assert H[0] == 5

    assert heapq.nsmallest(3, H) == [5, 6, 8]
    assert heapq.nlargest(3, H) == [78, 45, 21]
