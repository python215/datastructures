import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from myhashtable_1 import HashTable

def test_ht():

    t = HashTable(100)
    t["march 6"] = 310 # hash value will match with "March 17"
    t["march 7"] = 420
    t["march 8"] = 67
    t["march 17"] = 6345

    assert t['march 6'] == 310
    assert t['march 7'] == 420
    assert t['march 8'] == 67
    assert t['march 17'] == 6345

    del t['march 7']
    assert t['march 7'] == 0

