import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '../src')

from mystack import MyStack

def test_stack():
    mystack = MyStack()
    assert mystack.Length() == 0

    mystack.Push(1)
    mystack.Push(2)
    mystack.Push(3)
    mystack.Push(4)
    mystack.Push(5)
    assert mystack.Length() == 5
    assert mystack.Peek() == 5
    assert mystack.Pop() == 5
    assert mystack.Length() == 4

    assert mystack.GetStack() == [1,2,3,4]

