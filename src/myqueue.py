class MyQueue:
    def __init__(self):
        self.queue = list()

    def Length(self):
        return len(self.queue)

    def Push(self, item):
        if item not in self.queue:
            self.queue.insert(0, item)
            return True
        return False

    def Pop(self):
        if len(self.queue) > 0:
            return self.queue.pop()
        return "NO"

    def GetQ(self):
        return self.queue

    def Peek(self):
        return self.queue[-1]

