class HashTable:

    def __init__(self, max_val):
        self.MAX = max_val
        self.arr = [None for i in range(0, self.MAX)]

    def get_hash(self, key):
        h = 0
        for ch in key:
            h += ord(ch)
        return h%self.MAX

    def __setitem__(self, key, val):
        h = self.get_hash(key)
        self.arr[h] = val

    def __getitem__(self, key):
        h = self.get_hash(key)
        return self.arr[h]

    def get_len(self):
        return self.MAX

    def get_table(self):
        return self.arr

    def clear_table(self):
        self.arr = [None for i in range(0, self.MAX)]

    def __delitem__(self, key):
        h = self.get_hash(key)
        self.arr[h] = None

