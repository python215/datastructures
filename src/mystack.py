class MyStack:

    def __init__(self):
        self.stack = []

    def Push(self, item):
        if item not in self.stack:
            self.stack.append(item)
            return True
        return False

    def Pop(self):
        if len(self.stack) <= 0:
            return "NO"
        return self.stack.pop()

    def Peek(self):
        return self.stack[-1]

    def Length(self):
        return len(self.stack)

    def GetStack(self):
        return self.stack

