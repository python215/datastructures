import array as arr

class MyArray:
    def __init__(self, type_, val):
        self.my_array = arr.array(type_, val)

    def getArrayList(self):
        l = []
        for i in range(0, len(self.my_array)):
            l.append(self.my_array[i])
        return l

    def getValueAt(self, index):
        return self.my_array[index]

    def Append(self, val):
        self.my_array.append(val)

    def Insert(self, index, val):
        if index < 0 or index > len(self.my_array):
            return False
        self.my_array.insert(index, val)
        return True

    def Pop(self, pos=0):
        if pos == 0:
            self.my_array.pop()
        else:
            self.my_array.pop(pos)

    def Remove(self, item):
        self.my_array.remove(item)

    def Slice(self, pos1, pos2):
        return self.my_array[pos1:pos2]

    def Find(self, item):
        return self.my_array.index(item)

    def Update(self, index, val):
        self.my_array[index] = val

